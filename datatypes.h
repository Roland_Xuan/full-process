#ifndef DATATYPES_H
#define DATATYPES_H
#include<QObject>

//表示需要插入的数据类型

struct device_defect_info
{
    int Device_ID;//设备ID 酸洗7 热轧6
    int Defect_ID;//缺陷id
    QString Defect_Name;//缺陷名称
    QString Defect_Color;//缺陷颜色
};//缺陷信息


struct device_detect_detail{
    int Device_ID;// 设备ID
    QString Steel_ID;//钢板号码
    QString Detect_Time;//检测时间
    int Camera_ID;//相机号码
    int Defect_ID;//缺陷号码
    QString Defect_Image_Name;//纯缺陷图名称
    QString Defect_Min_Image_Name;//缺陷小图名称
    QString Defect_Max_Image_Name;//历史原图名称
    int Left_In_Max_Image;//图像上的坐标
    int Right_In_Max_Image;
    int Top_In_Max_Image;
    int Bottom_In_Max_Image;
    int Left_In_Steel;//钢板上的坐标
    int Right_In_Steel;
    int Top_In_Steel;
    int Bottom_In_Steel;
    int Defect_Area;//缺陷面积
    int Defect_no;//缺陷编号
};//区分表面

struct device_steel_info{
    int Device_ID;//设备ID
    QString Steel_ID;//钢板号码
    QString Detect_Time;//检测时间
    QString Steel_Parent_ID;//铸坯号
    QString Steel_Type;//钢种
    int Grade_ID;//等级ID
    QString Grade_Desc;//判级描述
    double Steel_Length_Source;//原始长宽厚
    double Steel_Width_Source;
    double Steel_Thick_Source;
    double Steel_Length_Detect;//检测长宽厚
    double Steel_Width_Detect;
    double Steel_Thick_Detect;
    int Defect_Count_Number;//缺陷数量
    int sequence;
};

struct device_steel_type_info{
    int Device_ID;//设备id
    QString Steel_Type;//钢种
};

#endif // DATATYPES_H
