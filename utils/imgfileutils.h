#ifndef IMGFILEUTILS_H
#define IMGFILEUTILS_H

#include <QObject>



// class ArNTImageOperator;
class ImgFileUtils : public QObject
{
    Q_OBJECT
public:
    explicit ImgFileUtils(QObject *parent = nullptr);
    ~ImgFileUtils();
    bool ReadPNGFromIMG(QString FilePath,QString saveFilePath,QString saveFileName,qint32 L,qint32 R,qint32 T,qint32 B);
private:
    // ArNTImageOperator *pImgOperator = nullptr;
};

#endif // IMGFILEUTILS_H
