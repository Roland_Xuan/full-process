#ifndef MYSQLCONNCTOR_H
#define MYSQLCONNCTOR_H

#include "database.h"

#include <QObject>
#include "../dataTypes.h"

class MySqlConnctor : public DataBase
{
    Q_OBJECT
public:
    explicit MySqlConnctor(QString dbname,QString hn,QString u,QString p);

signals:

public:
    //向tb_device_defect_info插入数据
    void insertTo_device_defect_info(QList<device_defect_info> list,const QSqlDatabase &db);

    void insertTo_device_steel_type_info(const device_steel_type_info &steelTypeInfo,const QSqlDatabase &db);

    void insertTo_device_steel_info(const device_steel_info &steelInfo,const QSqlDatabase &db);

    void insertTo_device_detect_detail(const QList<device_detect_detail> &defectList, bool Surface,const QSqlDatabase &db);

    QSqlDatabase openDataBase(QString connectionnName,QString Table);
};

#endif // MYSQLCONNCTOR_H
