#ifndef DATABASEADAPTER_H
#define DATABASEADAPTER_H

#include "database.h"

#include <QObject>


class DataBaseAdapter : public DataBase
{
    Q_OBJECT
public:



signals:

    // DataBase interface
public:
    QSqlDatabase openDataBase(QString connectionnName, QString Table);
};

#endif // DATABASEADAPTER_H
