#ifndef TCPTRANSFER_H
#define TCPTRANSFER_H

#include <QObject>
#include <QTimer>
#include <QTcpSocket>
#include <QSqlDatabase>
#include <QJsonObject>

class TcpTransFer : public QObject
{
    Q_OBJECT
public:
    explicit TcpTransFer(QObject *parent = nullptr);

    void  tryConnect();
    void onConnected();
    void onDisConnected();
    void onReadyToRead();
    void startConnection(const QString &host, quint16 port);
    void SendData(const QString & steelName );
    QString formatData(const QString & steelName );
    void CheckData();
    void SaveToFile(const QString &fileName, const QJsonObject &jsonObj);
    QJsonObject ReadToFile(const QString &fileName);

    void StartRunThread(QString name,QString parentName);
private:
    QTcpSocket *socket;
    QTimer *reconnectTimer;
    QTimer *CheckDataTimer;
    QString host;
    quint16 port;
    int retryCount;
    const int maxRetry;

    QSqlDatabase db;
signals:
    void dataIsReady(QString,QString);
};

#endif // TCPTRANSFER_H
