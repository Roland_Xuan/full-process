#ifndef FINDMSSQLDATATHREAD_H
#define FINDMSSQLDATATHREAD_H

#include <QObject>
#include <QSqlDatabase>
#include <QThread>
#include "../utils/imgfileutils.h"

class FindMssqlDataThread : public QThread
{
    Q_OBJECT
public:
    FindMssqlDataThread(QString steel ,QString parent);


    // QThread interface
protected:
    void run();
    QString PNGFileSavePath(int sequenceNum, int CamNo, int ImgIndex,int defectNo);
    QSqlDatabase mssqlDb;
private:
    QString SteelName;
    QString SteelParentName;
    // ImgFileUtils utils;

};

#endif // FINDMSSQLDATATHREAD_H
