#include "findmssqldatathread.h"
#include "../database/sqlserverconnector.h"
#include "../database/mysqlconnctor.h"
#include "../datatypes.h"
#include <QSqlQuery>
#include <QSqlRecord>
#include <QDate>

FindMssqlDataThread::FindMssqlDataThread(QString steel ,QString parent)
    :SteelName(steel),SteelParentName(parent)
{



}

void FindMssqlDataThread::run()
{


    SqlServerConnector sqlserveConn("QODBC","172.31.202.2", "ARNTUSER","ARNTUSER");
    // SqlServerConnector sqlserveConn("QODBC","127.0.0.1", "ARNTUSER","ARNTUSER");
    QSqlDatabase MSSQLDB = sqlserveConn.openDataBase("conn_Mssql","ConfigCenter");

    MySqlConnctor mysqlConn("QMYSQL","10.10.30.170","root","nercar");
    // MySqlConnctor mysqlConn("QMYSQL","127.0.0.1","root","123456");
    QSqlDatabase  MYSqlDB= mysqlConn.openDataBase("conn_Mysql","db_full_process");

    {
        QSqlQuery queryDefect(MSSQLDB);
        //查询缺陷类别
        QString sql_SelectDefectType = R"(
        SELECT Name, Class, Red, Green, Blue
        FROM [ConfigCenter].[dbo].[DefectClass]
        )";
        queryDefect.exec(sql_SelectDefectType);
        QList<device_defect_info> device_defect_info_List;
        while(queryDefect.next()){
            device_defect_info element;
            element.Device_ID = 7;
            element.Defect_ID = queryDefect.record().value("Class").toInt();
            element.Defect_Name = queryDefect.record().value("Name").toString();
            int r = queryDefect.record().value("Red").toInt();
            int g = queryDefect.record().value("Green").toInt();
            int b = queryDefect.record().value("Blue").toInt();
            element.Defect_Color = QString("#%1%2%3")
                                       .arg(r, 2, 16, QChar('0'))
                                       .arg(g, 2, 16, QChar('0'))
                                       .arg(b, 2, 16, QChar('0'))
                                       .toUpper();
            device_defect_info_List << element;
        }
        mysqlConn.insertTo_device_defect_info(device_defect_info_List,MYSqlDB);
    }
    MSSQLDB.close();
    //提前将数据查询好并插入数据
    device_steel_type_info steel_type_info;
    device_steel_info steel_info;
    {
        QString sql_SteelInfo = R"(
        Use [SteelRecord]
        SELECT
        steel.SequeceNo,
        steel.TopLen,
        steel.TopWidth,
        (steel.TopDefectNum + steel.BottomDefectNum) as DefectNum,
        steelId.AddTime,
        steelId.Length,
        steelid.Width,
        steelId.Thick,
        SteelID.SteelType,
        steelGrade.Grade
        FROM
        [SteelRecord].[dbo].[steel] as steel,
        [SteelRecord].[dbo].[SteelID] as steelId,
        [SteelDetectLv].[dbo].[SteelGradeInfo] as steelGrade
        where
        steel.SteelID = steelId.ID
        and
        steelGrade.SteelName = steel.SteelID
        and
        SteelID = :SteelName;
    )";

        MSSQLDB = sqlserveConn.openDataBase("conn_Mssql","SteelRecord");

        QSqlQuery steelInfoQuery(MSSQLDB);
        steelInfoQuery.prepare(sql_SteelInfo);
        steelInfoQuery.bindValue(":SteelName",SteelName);
        if(!steelInfoQuery.exec()){
            qDebug()<<"执行钢板信息查询失败!";
            return;
        }
        while(steelInfoQuery.next()){
            steel_type_info.Device_ID = 7;
            steel_type_info.Steel_Type = steelInfoQuery.record().value("SteelType").toString();

            steel_info.Device_ID = 7;
            steel_info.Steel_ID = SteelName;
            steel_info.Steel_Parent_ID = SteelParentName;
            steel_info.Detect_Time = steelInfoQuery.record().value("AddTime").toString();
            steel_info.Steel_Type = steelInfoQuery.record().value("SteelType").toString();
            steel_info.Grade_ID = steelInfoQuery.record().value("Grade").toInt();
            steel_info.Grade_Desc = "";
            steel_info.Steel_Length_Source = steelInfoQuery.record().value("Length").toDouble();
            steel_info.Steel_Thick_Source = steelInfoQuery.record().value("Thick").toDouble();
            steel_info.Steel_Width_Source = steelInfoQuery.record().value("Width").toDouble();
            steel_info.Steel_Length_Detect = steelInfoQuery.record().value("TopLen").toDouble();
            steel_info.Steel_Width_Detect = steelInfoQuery.record().value("TopWidth").toDouble();
            steel_info.Steel_Thick_Detect = 0;
            steel_info.Defect_Count_Number = steelInfoQuery.record().value("DefectNum").toInt();
            steel_info.sequence = steelInfoQuery.record().value("SequeceNo").toInt();

        }
        qDebug()<<"查询的钢卷数据"<<SteelName<<" 铸坯号: " << SteelParentName;
        mysqlConn.insertTo_device_steel_type_info(steel_type_info,MYSqlDB);
        mysqlConn.insertTo_device_steel_info(steel_info,MYSqlDB);
}



    MSSQLDB.close();

    //查询缺陷数据
    QList<device_detect_detail> detect_detail_Top_List;
    QList<device_detect_detail> detect_detail_Bottom_List;
    {
        MSSQLDB = sqlserveConn.openDataBase("conn_Mssql","ClientDefectDB1");
        //区分上下表面存储缺陷信息
        QString str = R"(
        Use [ClientDefectDB%1]
        SELECT DISTINCT
        steel.SteelID,
        steelId.AddTime,
        defect.CameraNo,
        defect.DefectNo,
        defect.SteelNo,
        defect.LeftInImg,
        defect.RightInImg,
        defect.TopInImg,
        defect.BottomInImg,
        defect.LeftInSteel,
        defect.RightInSteel,
        defect.TopInSteel,
        defect.BottomInSteel,
        defect.Class,
        defect.Area,
        defect.ImageIndex
        FROM
        [dbo].[Defect] as defect,
        [SteelRecord].[dbo].[steel] as steel,
        [ConfigCenter].[dbo].[DefectClass] as defectClz,
        [SteelRecord].[dbo].[SteelID] as steelID
        WHERE
        defect.SteelNo = steel.SequeceNo
        and
        steel.SteelID = steelId.ID
        and
        steel.SteelID = '%2';
        )";
        QSqlQuery defectQuery(MSSQLDB);
        for (int i = 1; i <= 4; ++i) {
            QString sql_str = str.arg(i).arg(SteelName);
            defectQuery.exec(sql_str);
            while(defectQuery.next()){
                device_detect_detail defectDetail;
                defectDetail.Device_ID = 7;
                defectDetail.Steel_ID =  defectQuery.record().value("SteelID").toString();
                defectDetail.Detect_Time = defectQuery.record().value("AddTime").toString();
                defectDetail.Camera_ID = defectQuery.record().value("CameraNo").toInt();
                defectDetail.Defect_no = defectQuery.record().value("DefectNo").toInt();
                int Sequence = defectQuery.record().value("SteelNo").toInt();
                defectDetail.Left_In_Max_Image =  defectQuery.record().value("LeftInImg").toDouble();
                defectDetail.Right_In_Max_Image = defectQuery.record().value("RightInImg").toDouble();
                defectDetail.Top_In_Max_Image = defectQuery.record().value("TopInImg").toDouble();
                defectDetail.Bottom_In_Max_Image = defectQuery.record().value("BottomInImg").toDouble();
                defectDetail.Left_In_Steel = defectQuery.record().value("LeftInSteel").toDouble();
                defectDetail.Right_In_Steel = defectQuery.record().value("RightInSteel").toDouble();
                defectDetail.Top_In_Steel = defectQuery.record().value("TopInSteel").toDouble();
                defectDetail.Bottom_In_Steel = defectQuery.record().value("BottomInSteel").toDouble();
                defectDetail.Defect_ID = defectQuery.record().value("Class").toInt();
                defectDetail.Defect_Area = defectQuery.record().value("Area").toInt();
                int Imgidx = defectQuery.record().value("ImageIndex").toInt();
                defectDetail.Defect_Min_Image_Name = QString("CamDefectImage%1/%2/%3.img").arg(defectDetail.Camera_ID).arg(Sequence).arg(Imgidx,4,10,QChar('0'));
                defectDetail.Defect_Image_Name =  QString("%1_%2.png").arg(defectDetail.Camera_ID).arg(Imgidx); //缺陷图片名称
                if(defectDetail.Camera_ID <= 2){
                    detect_detail_Top_List<<defectDetail;
                }else{
                    detect_detail_Bottom_List<<defectDetail;
                }

                //QString ImgTotalPath = QString("////192.168.2.100//CamDefectImage%1//%2//%3.img").arg(defectDetail.Camera_ID).arg(Sequence).arg(Imgidx,4,10,QChar('0'));
                //QString pngSave = PNGFileSavePath(Sequence,defectDetail.Camera_ID,Imgidx,defectDetail.Defect_no);
                //utils.ReadPNGFromIMG(ImgTotalPath,pngSave,defectDetail.Defect_Image_Name,defectDetail.Left_In_Max_Image,defectDetail.Right_In_Max_Image,defectDetail.Top_In_Max_Image,defectDetail.Bottom_In_Max_Image);
            }
        }
        mysqlConn.insertTo_device_detect_detail(detect_detail_Top_List,true,MYSqlDB);
        mysqlConn.insertTo_device_detect_detail(detect_detail_Bottom_List,false,MYSqlDB);
    }
    MSSQLDB.close();
    MSSQLDB.removeDatabase("conn_Mssql");
    MYSqlDB.close();
    MYSqlDB.removeDatabase("conn_Mysql");





}

QString FindMssqlDataThread::PNGFileSavePath(int sequenceNum, int CamNo, int ImgIndex, int defectNo)
{
    //  \\10.10.30.170\Images\7\DefectImage
    QString targetPath = "////Images10.10.30.170//7//DefectImage";

    QString currentDate = QDate::currentDate().toString("yyyyMMdd");
    targetPath += "//" + currentDate;
    // int SelectDbIndex = 0;
    //3. 根据规则追加 1 或 2
    if (CamNo == 1 || CamNo == 2) {
        targetPath += "//1";
        // SelectDbIndex = 1;
    } else {
        targetPath += "//2";
        // SelectDbIndex = 2;
    }
    targetPath += QString("//%1").arg(sequenceNum);
    return targetPath;
}
